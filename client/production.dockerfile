FROM node:10.16 as builder
ARG REACT_APP_ENV
ENV REACT_APP_ENV $REACT_APP_ENV

WORKDIR /app
COPY ./package*.json ./
RUN npm install
COPY . .



ARG REACT_APP_URL
ENV REACT_APP_URL $REACT_APP_URL

ARG REACT_APP_API_URL
ENV REACT_APP_API_URL $REACT_APP_API_URL

ARG REACT_APP_AUTH_URL
ENV REACT_APP_AUTH_URL $REACT_APP_AUTH_URL

RUN npm run build

FROM nginx
# EXPOSE 3000
COPY ./nginx/admin/default.conf /etc/nginx/conf.d/default.conf
COPY --from=builder /app/build /usr/share/nginx/html