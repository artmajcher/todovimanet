import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Message } from "semantic-ui-react";
import { mapErrorToList } from "../../_helpers/errorMapper";
import { history } from "../../_helpers/rootReducer";
import { clear } from "./redux/actions";
const Errors = () => {
  const dispatch = useDispatch();
  const { error } = useSelector(state => ({
    ...state.errorsReducer
  }));
  history.listen((location, action) => {
    dispatch(clear());
  });

  return (
    <>
      {error && (
        <Message negative header="Errors" list={mapErrorToList(error)} />
      )}
    </>
  );
};

export default Errors;
