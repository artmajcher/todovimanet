import { FAILED, CLEAR } from "./types";

const initialState = {
  loading: false,
  loaded: false,
  servers: null,
  error: null
};

export default function errorsReducer(state = initialState, action) {
  switch (action.type) {
    case CLEAR:
      return {
        ...state,
        hasError: false,
        error: null
      };

    case FAILED:
      return {
        hasError: true,
        unauthorized: action.payload.error.status === 401,
        error: action.payload.error.data
      };

    default:
      return state;
  }
}
