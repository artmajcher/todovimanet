import { FAILED, CLEAR } from "./types";

export const clear = () => {
  return dispatch => {
    dispatch({
      type: CLEAR
    });
  };
};

export const clearErrors = () => {
  return dispatch => {
    dispatch({
      type: CLEAR
    });
  };
};

export const setError = error => ({
  type: FAILED,
  payload: {
    error
  }
});
