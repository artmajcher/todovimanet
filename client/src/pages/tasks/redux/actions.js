import axios from "axios";
import {
  REQUESTED,
  GET_ALL_DONE,
  GET_DONE,
  POST_DONE,
  CLEAR,
  ADD_DONE
} from "./types";
import api from "../../../_helpers/api";
import { setError, clearErrors } from "../../errors/redux/actions";

export const getTaskGroups = () => {
  return dispatch => {
    dispatch(request());
    axios
      .get(api.tasks)
      .then(res => {
        dispatch(getAllCompleted(res.data));
      })
      .catch(error => {
        if (error.response) {
          dispatch(setError(error.response));
        }
      });
  };
};
export const getTaskGroup = id => {
  return dispatch => {
    dispatch(request());
    axios
      .get(api.tasks + "/" + id)
      .then(res => {
        dispatch(getCompleted(res.data));
      })
      .catch(error => {
        if (error.response) {
          dispatch(setError(error.response));
        }
      });
  };
};
export const editTaskGroup = data => {
  return dispatch => {
    dispatch(request());
    axios
      .put(api.tasks, data)
      .then(res => {
        dispatch(clearErrors());
        dispatch(submitCompleted(res.data));
      })
      .catch(error => {
        if (error.response) {
          dispatch(setError(error.response));
        }
        dispatch(getTaskGroup(data.id));
      });
  };
};
export const addTaskGroup = data => {
  return dispatch => {
    dispatch(request());
    axios
      .post(api.tasks, data)
      .then(res => {
        dispatch(addCompleted(res.data));
      })
      .catch(error => {
        if (error.response) {
          dispatch(setError(error.response));
        }
      });
  };
};
export const removeTaskGroup = id => {
  return dispatch => {
    dispatch(request());
    axios
      .delete(api.tasks + "/" + id)
      .then(() => {
        dispatch(getTaskGroups());
      })
      .catch(error => {
        if (error.response) {
          dispatch(setError(error.response));
        }
      });
  };
};
export const clear = () => {
  return dispatch => {
    dispatch({
      type: CLEAR
    });
  };
};

const getAllCompleted = data => ({
  type: GET_ALL_DONE,
  payload: {
    ...data
  }
});

const getCompleted = data => ({
  type: GET_DONE,
  payload: {
    ...data
  }
});
const submitCompleted = data => ({
  type: POST_DONE,
  payload: {
    ...data
  }
});

const addCompleted = data => ({
  type: ADD_DONE,
  payload: {
    ...data
  }
});
const request = () => ({
  type: REQUESTED
});
