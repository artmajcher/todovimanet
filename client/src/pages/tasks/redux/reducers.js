import {
  REQUESTED,
  GET_ALL_DONE,
  POST_DONE,
  GET_DONE,
  CLEAR,
  ADD_DONE
} from "./types";

const initialState = {
  loading: false,
  loaded: false,
  taskGroups: null
};

export default function tasksReducer(state = initialState, action) {
  switch (action.type) {
    case CLEAR:
      return {
        ...state,
        loading: false,
        saved: false,
        added: false
      };
    case REQUESTED:
      return {
        ...state,
        loading: true,
        loaded: false,
        taskGroup: null
      };
    case GET_ALL_DONE:
      return {
        ...state,
        loading: false,
        loaded: true,
        ...action.payload
      };
    case GET_DONE:
      return {
        ...state,
        loading: false,
        loaded: true,
        taskGroup: action.payload
      };
    case POST_DONE:
      return {
        ...state,
        loading: false,
        loaded: true,
        saved: true
      };
    case ADD_DONE:
      return {
        ...state,
        loading: false,
        loaded: true,
        added: true
      };

    default:
      return state;
  }
}
