import React from "react";
import { useSelector } from "react-redux";
import { Button, Form, Input } from "formik-semantic-ui";
import * as Yup from "yup";

const TaskGroupForm = ({ initialValues, submitHandler, buttonText }) => {
  const { loading } = useSelector(state => ({
    ...state.usersReducer
  }));

  return (
    <Form
      validateOnChange
      validateOnBlur
      initialValues={initialValues}
      onSubmit={(values, formikApi) => {
        const data = Object.assign(initialValues, values);
        submitHandler(data);
        formikApi.setSubmitting(false);
      }}
      validationSchema={Yup.object().shape({
        name: Yup.string().required()
      })}
    >
      <Input
        label="Group name"
        name="name"
        inputProps={{ placeholder: "Name" }}
      />
      <Button.Submit disabled={loading} loading={loading}>
        {buttonText}
      </Button.Submit>
    </Form>
  );
};

export default TaskGroupForm;
