import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Header, Grid, Segment, Divider } from "semantic-ui-react";
import TaskForm from "./TaskForm";
import { getUsers } from "../users/redux/actions";
import TasksList from "./TasksList";
import TaskGroupForm from "./TaskGroupForm";

const TaskGroupDetails = ({ taskGroup, submitHandler }) => {
  const dispatch = useDispatch();
  const { users } = useSelector(state => ({
    ...state.usersReducer
  }));
  const [tasks, setTasks] = useState([]);

  const useMountEffect = fun => useEffect(fun, []);

  useMountEffect(() => {
    if (taskGroup) setTasks([...taskGroup.tasks]);
    if (!users) dispatch(getUsers());
  });

  const handleTaskAdd = newTask => {
    setTasks([...tasks, newTask]);
  };
  const handleTaskRemove = taskId => {
    setTasks([...tasks.filter((task, i) => i !== taskId)]);
  };
  const handleTaskGroupSave = data => {
    submitHandler({ ...data, tasks: tasks });
  };

  const ininitalValues = taskGroup
    ? taskGroup
    : {
        name: ""
      };
  const parsedUsers =
    users &&
    users.map(u => ({
      value: u.id,
      text: `${u.firstName}  ${u.lastName}`
    }));

  return (
    <>
      <Grid>
        <Grid.Row>
          <Grid.Column width="4">
            <Segment>
              <Header>Create Task</Header>
              <TaskForm users={parsedUsers} submitHandler={handleTaskAdd} />
            </Segment>
          </Grid.Column>
          <Grid.Column width="12">
            <Header>List of Tasks</Header>
            <TasksList tasks={tasks} deleteHandler={handleTaskRemove} />
          </Grid.Column>
        </Grid.Row>
      </Grid>
      <Divider hidden />
      <TaskGroupForm
        initialValues={ininitalValues}
        buttonText="Save"
        submitHandler={handleTaskGroupSave}
      />
    </>
  );
};

export default TaskGroupDetails;
