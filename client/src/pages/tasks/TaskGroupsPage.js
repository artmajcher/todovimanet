import React, { useEffect, useContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Message, Button, Icon } from "semantic-ui-react";
import TasksTable from "./TaskGroupsTable";
import { NavLink } from "react-router-dom";
import { getTaskGroups, removeTaskGroup } from "./redux/actions";
import NavContext from "../../contexts/NavContext";
const TaskGroupsPage = () => {
  const context = useContext(NavContext);
  const dispatch = useDispatch();
  const { loading, loaded, taskGroups } = useSelector(state => ({
    ...state.tasksReducer
  }));

  const useMountEffect = fun => useEffect(fun, []);

  useMountEffect(() => {
    dispatch(getTaskGroups());
  });

  const handleTaskDelete = id => {
    dispatch(removeTaskGroup(id));
  };

  return (
    <>
      {loading && !loaded && <Message>Loading ...</Message>}
      {taskGroups && (
        <>
          <Button
            icon
            primary
            size="tiny"
            as={NavLink}
            to={context.tasksUrl + "/add"}
          >
            <Icon name="add" /> New Task Group
          </Button>

          <TasksTable data={taskGroups} deleteHandler={handleTaskDelete} />
        </>
      )}
    </>
  );
};

export default TaskGroupsPage;
