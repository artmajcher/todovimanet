import React, { useEffect, useContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Message } from "semantic-ui-react";
import { getTaskGroup, clear, editTaskGroup } from "./redux/actions";
import { NavLink } from "react-router-dom";
import NavContext from "../../contexts/NavContext";
import TaskGroupDetails from "./TaskGroupDetails";

const TaskGroupEditPage = props => {
  const dispatch = useDispatch();
  const context = useContext(NavContext);
  const { loading, loaded, taskGroup, saved } = useSelector(state => ({
    ...state.tasksReducer
  }));
  const useMountEffect = fun => useEffect(fun, []);
  const useDoneEffect = fun => useEffect(fun, [saved]);

  useMountEffect(() => {
    const id = props.match.params.id;
    dispatch(clear());
    dispatch(getTaskGroup(id));
  });

  useDoneEffect(() => {
    const id = props.match.params.id;
    if (saved) dispatch(getTaskGroup(id));
  });

  const handleChange = data => {
    dispatch(clear());
    dispatch(editTaskGroup({ ...data }));
  };

  return (
    <>
      {loading && !loaded && <Message>Loading ...</Message>}
      {saved && (
        <Message positive>
          Changes have been saved.{" "}
          <NavLink to={context.tasksUrl} exact position="right">
            Back to the list
          </NavLink>
        </Message>
      )}
      {taskGroup && (
        <TaskGroupDetails taskGroup={taskGroup} submitHandler={handleChange} />
      )}
    </>
  );
};

export default TaskGroupEditPage;
