import React from "react";
import { useSelector } from "react-redux";
import { Button, Form, Input, Dropdown } from "formik-semantic-ui";
import * as Yup from "yup";
import FormikDatePicker from "../../components/DataPicker/DataPicker";
import {
  getDropDownListItems,
  getUserTaskStatusName,
  userTaskStatus
} from "../../_helpers/enums";
import dayjs from "dayjs";

const TaskForm = ({ users, submitHandler, buttonText }) => {
  const { loading } = useSelector(state => ({
    ...state.usersReducer
  }));

  return (
    <Form
      size="tiny"
      validateOnChange
      initialValues={{
        name: "",
        deadline: "",
        status: "",
        userId: ""
      }}
      onSubmit={(values, formikApi) => {
        values.deadline = dayjs(values.deadline);
        const selectedUser = users.find(u => u.value === values.userId);
        values.user = selectedUser ? selectedUser.text : undefined;

        submitHandler(values);
        formikApi.resetForm();
        formikApi.setSubmitting(false);
      }}
      validationSchema={Yup.object().shape({
        name: Yup.string().required(),
        deadline: Yup.string().required(),
        status: Yup.string().required()
      })}
    >
      <Input label="Name" name="name" inputProps={{ placeholder: "Name" }} />
      <FormikDatePicker
        label="Deadline"
        name="deadline"
        inputProps={{
          renderMonthElement: props => (
            <FormikDatePicker.YearMonthSelector {...props} />
          )
        }}
      />
      <Dropdown
        label="Status"
        name="status"
        inputProps={{
          placeholder: "Status ..."
        }}
        options={getDropDownListItems(
          0,
          userTaskStatus.Completed,
          getUserTaskStatusName
        )}
      />
      <Dropdown
        label="User"
        name="userId"
        inputProps={{
          placeholder: "Choose user..."
        }}
        options={users}
      />

      <Button.Submit size="tiny" secondary disabled={loading} loading={loading}>
        Create Task
      </Button.Submit>
    </Form>
  );
};

export default TaskForm;
