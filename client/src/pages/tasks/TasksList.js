import React from "react";

import { Card, Button } from "semantic-ui-react";
import { getUserTaskStatusName } from "../../_helpers/enums";
import dayjs from "dayjs";
import { DATE_FORMAT } from "../../_helpers/dateFormats";

const TasksList = ({ tasks, deleteHandler }) => {
  return (
    <Card.Group>
      {tasks &&
        tasks.length > 0 &&
        tasks.map((task, i) => (
          <Card key={i}>
            <Card.Content>
              <Card.Header>
                {task.name} - {getUserTaskStatusName(task.status)}
              </Card.Header>
              <Card.Meta>{dayjs(task.deadline).format(DATE_FORMAT)}</Card.Meta>
              <Card.Description>{task.user}</Card.Description>
            </Card.Content>
            <Card.Content extra>
              <div className="ui two buttons">
                <Button size="small" onClick={() => deleteHandler(i)}>
                  Remove
                </Button>
              </div>
            </Card.Content>
          </Card>
        ))}
    </Card.Group>
  );
};

export default TasksList;
