import React, { useContext, useState, useEffect } from "react";
import _ from "lodash";
import { NavLink } from "react-router-dom";
import { Table, Button, Icon } from "semantic-ui-react";
import NavContext from "../../contexts/NavContext";
import ConfirmCallback from "../../components/ConfirmCallback/ConfirmCallback";

const TaskGroupsTable = ({ data, deleteHandler }) => {
  const context = useContext(NavContext);
  const [column, setColumn] = useState([]);
  const [sortedData, setSortedData] = useState([]);
  const [direction, setDirection] = useState([]);

  const useMountEffect = fun => useEffect(fun, [data]);

  useMountEffect(() => {
    setSortedData(data);
  });

  const handleSort = clickedColumn => () => {
    if (column !== clickedColumn) {
      setColumn(clickedColumn);
      setSortedData(_.sortBy(data, [clickedColumn]));
      setDirection("ascending");
      return;
    }
    setSortedData(sortedData.reverse());
    setDirection(direction === "ascending" ? "descending" : "ascending");
  };

  return (
    <Table sortable celled padded stackable size="small">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell
            sorted={column === "name" ? direction : null}
            onClick={handleSort("name")}
          >
            Name
          </Table.HeaderCell>
          <Table.HeaderCell
            sorted={column === "total" ? direction : null}
            onClick={handleSort("total")}
          >
            Number of Tasks
          </Table.HeaderCell>
          <Table.HeaderCell></Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        {sortedData &&
          sortedData.length > 0 &&
          sortedData.map(row => (
            <Table.Row key={row.id}>
              <Table.Cell>{row.name}</Table.Cell>
              <Table.Cell>{row.tasksCount}</Table.Cell>

              <Table.Cell collapsing>
                <Button
                  icon
                  primary
                  size="tiny"
                  as={NavLink}
                  to={context.tasksUrl + "/" + row.id}
                >
                  <Icon name="edit" /> Edit
                </Button>
                <ConfirmCallback
                  buttonText="Remove"
                  callback={() => deleteHandler(row.id)}
                  content={
                    "Are you sure you want to delete task " + row.name + " ?"
                  }
                />
              </Table.Cell>
            </Table.Row>
          ))}
      </Table.Body>
    </Table>
  );
};

export default TaskGroupsTable;
