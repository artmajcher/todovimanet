import React, { useEffect, useContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Message } from "semantic-ui-react";
import { clear, addTaskGroup } from "./redux/actions";
import { NavLink } from "react-router-dom";
import NavContext from "../../contexts/NavContext";
import TaskGroupDetails from "./TaskGroupDetails";

const TaskGroupAddPage = props => {
  const dispatch = useDispatch();
  const context = useContext(NavContext);
  const { loading, loaded, added } = useSelector(state => ({
    ...state.tasksReducer
  }));

  const useMountEffect = fun => useEffect(fun, []);

  useMountEffect(() => {
    dispatch(clear());
  });

  const handleAdd = data => {
    dispatch(addTaskGroup(data));
  };

  return (
    <>
      {loading && !loaded && <Message>Loading ...</Message>}
      {added && (
        <Message positive>
          New task added.{" "}
          <NavLink to={context.tasksUrl} exact position="right">
            Back to the list
          </NavLink>
        </Message>
      )}
      {!added && <TaskGroupDetails submitHandler={handleAdd} />}
    </>
  );
};

export default TaskGroupAddPage;
