import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import { Table, Button, Icon } from "semantic-ui-react";
import NavContext from "../../contexts/NavContext";
import ConfirmCallback from "../../components/ConfirmCallback/ConfirmCallback";

const UsersTable = ({ data, deleteHandler }) => {
  const context = useContext(NavContext);
  return (
    <Table celled padded stackable size="small">
      <Table.Header>
        <Table.Row>
          <Table.HeaderCell>First Name</Table.HeaderCell>
          <Table.HeaderCell>Last Name</Table.HeaderCell>
          <Table.HeaderCell></Table.HeaderCell>
        </Table.Row>
      </Table.Header>

      <Table.Body>
        {data &&
          data.length > 0 &&
          data.map(row => (
            <Table.Row key={row.id}>
              <Table.Cell>{row.firstName}</Table.Cell>
              <Table.Cell>{row.lastName}</Table.Cell>

              <Table.Cell collapsing>
                <Button
                  icon
                  primary
                  size="tiny"
                  as={NavLink}
                  to={context.usersUrl + "/" + row.id}
                >
                  <Icon name="edit" /> Edit
                </Button>
                <ConfirmCallback
                  buttonText="Remove"
                  callback={() => deleteHandler(row.id)}
                  content={
                    "Are you sure you want to delete user " +
                    row.firstName +
                    " ?"
                  }
                />
              </Table.Cell>
            </Table.Row>
          ))}
      </Table.Body>
    </Table>
  );
};

export default UsersTable;
