import React, { useEffect, useContext } from "react";
//import contactPageContext from "../../contexts/contactPageContext";
import { useDispatch, useSelector } from "react-redux";
import { Message, Button, Icon } from "semantic-ui-react";
import UsersTable from "./UsersTable";
import { NavLink } from "react-router-dom";
import { getUsers, removeUser } from "./redux/actions";
import NavContext from "../../contexts/NavContext";
const UsersPage = () => {
  const context = useContext(NavContext);
  const dispatch = useDispatch();
  const { loading, loaded, users } = useSelector(state => ({
    ...state.usersReducer
  }));

  const useCountChange = fun => useEffect(fun, []);

  useCountChange(() => {
    dispatch(getUsers());
  });

  const handleUserDelete = data => {
    dispatch(removeUser(data));
  };

  return (
    <>
      {loading && !loaded && <Message>Trwa wczytywanie danych ...</Message>}
      {users && (
        <>
          <Button
            icon
            primary
            size="tiny"
            as={NavLink}
            to={context.usersUrl + "/add"}
          >
            <Icon name="add" /> New User
          </Button>

          <UsersTable data={users} deleteHandler={handleUserDelete} />
        </>
      )}
    </>
  );
};

export default UsersPage;
