export const REQUESTED = "USER_REQUESTED";
export const CLEAR = "USER_CLEAR";
export const GET_ALL_DONE = "GET_ALL_USER_DETAIL_DONE";
export const GET_DONE = "GET_USER_DETAIL_DONE";
export const POST_DONE = "POST_USER_DETAIL_DONE";
export const ADD_DONE = "ADD_USER_DETAIL_DONE";
