import axios from "axios";
import {
  REQUESTED,
  GET_ALL_DONE,
  GET_DONE,
  POST_DONE,
  CLEAR,
  ADD_DONE
} from "./types";
import api from "../../../_helpers/api";
import { setError, clearErrors } from "../../errors/redux/actions";

export const getUsers = () => {
  return dispatch => {
    dispatch(request());
    axios
      .get(api.users)
      .then(res => {
        dispatch(getAllCompleted(res.data));
      })
      .catch(error => {
        if (error.response) {
          dispatch(setError(error.response));
        }
      });
  };
};
export const getUser = id => {
  return dispatch => {
    dispatch(request());
    axios
      .get(api.users + "/" + id)
      .then(res => {
        dispatch(getCompleted(res.data));
      })
      .catch(error => {
        if (error.response) {
          dispatch(setError(error.response));
        }
      });
  };
};
export const editUser = data => {
  return dispatch => {
    dispatch(request());
    axios
      .put(api.users, data)
      .then(res => {
        dispatch(clearErrors());
        dispatch(submitCompleted(res.data));
      })
      .catch(error => {
        if (error.response) {
          dispatch(setError(error.response));
        }
        dispatch(getUser(data.id));
      });
  };
};
export const addUser = data => {
  return dispatch => {
    dispatch(request());
    axios
      .post(api.users, data)
      .then(res => {
        dispatch(addCompleted(res.data));
      })
      .catch(error => {
        if (error.response) {
          dispatch(setError(error.response));
        }
      });
  };
};
export const removeUser = id => {
  return dispatch => {
    dispatch(request());
    axios
      .delete(api.users + "/" + id)
      .then(() => {
        dispatch(getUsers());
      })
      .catch(error => {
        if (error.response) {
          dispatch(setError(error.response));
        }
      });
  };
};
export const clear = () => {
  return dispatch => {
    dispatch({
      type: CLEAR
    });
  };
};

const getAllCompleted = data => ({
  type: GET_ALL_DONE,
  payload: {
    ...data
  }
});

const getCompleted = data => ({
  type: GET_DONE,
  payload: {
    ...data
  }
});
const submitCompleted = data => ({
  type: POST_DONE,
  payload: {
    ...data
  }
});

const addCompleted = data => ({
  type: ADD_DONE,
  payload: {
    ...data
  }
});
const request = () => ({
  type: REQUESTED
});
