import React, { useEffect, useContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Header, Message } from "semantic-ui-react";
import { getUser, clear, editUser } from "./redux/actions";
import { NavLink } from "react-router-dom";
import NavContext from "../../contexts/NavContext";
import UserForm from "./UserForm";

const UserEditPage = props => {
  const dispatch = useDispatch();
  const context = useContext(NavContext);
  const { loading, loaded, user, saved } = useSelector(state => ({
    ...state.usersReducer
  }));
  const useMountEffect = fun => useEffect(fun, []);
  const useDoneEffect = fun => useEffect(fun, [saved]);

  useMountEffect(() => {
    const id = props.match.params.id;
    dispatch(clear());
    dispatch(getUser(id));
  });

  useDoneEffect(() => {
    const id = props.match.params.id;
    if (saved) dispatch(getUser(id));
  });

  const handleChange = data => {
    dispatch(clear());
    dispatch(editUser({ ...data }));
  };

  return (
    <>
      <Header>Edit User</Header>
      {loading && !loaded && <Message>Trwa wczytywanie danych ...</Message>}
      {saved && (
        <Message positive>
          Changes have been saved.{" "}
          <NavLink to={context.homeUrl} exact position="right">
            Back to the list
          </NavLink>
        </Message>
      )}
      {user && (
        <UserForm
          initialValues={user}
          submitHandler={handleChange}
          buttonText="Save User"
        />
      )}
    </>
  );
};

export default UserEditPage;
