import React, { useEffect, useContext } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Header, Message } from "semantic-ui-react";
import { clear, addUser } from "./redux/actions";
import { NavLink } from "react-router-dom";
import NavContext from "../../contexts/NavContext";
import UserForm from "./UserForm";

const UserAddPage = props => {
  const dispatch = useDispatch();
  const context = useContext(NavContext);
  const { loading, loaded, added } = useSelector(state => ({
    ...state.usersReducer
  }));

  const useMountEffect = fun => useEffect(fun, []);

  useMountEffect(() => {
    dispatch(clear());
  });

  const handleChange = data => {
    dispatch(addUser({ ...data }));
  };
  return (
    <>
      <Header>Add User</Header>
      {loading && !loaded && <Message>Loading ...</Message>}
      {added && (
        <Message positive>
          New user added.{" "}
          <NavLink to={context.homeUrl} exact position="right">
            Back to the list
          </NavLink>
        </Message>
      )}
      {!added && (
        <UserForm
          initialValues={{
            firstName: "",
            lastName: ""
          }}
          submitHandler={handleChange}
          buttonText="Add User"
        />
      )}
    </>
  );
};

export default UserAddPage;
