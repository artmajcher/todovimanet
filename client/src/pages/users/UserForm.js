import React from "react";
import { useSelector } from "react-redux";
import { Button, Form, Input } from "formik-semantic-ui";
import * as Yup from "yup";

const UserForm = ({ initialValues, submitHandler, buttonText }) => {
  const { loading } = useSelector(state => ({
    ...state.usersReducer
  }));

  return (
    <Form
      validateOnChange
      validateOnBlur
      initialValues={initialValues}
      onSubmit={(values, formikApi) => {
        const data = Object.assign(initialValues, values);
        submitHandler({ ...data });
        formikApi.setSubmitting(false);
      }}
      validationSchema={Yup.object().shape({
        firstName: Yup.string().required(),
        lastName: Yup.string().required()
      })}
    >
      <Input
        label="First Name"
        name="firstName"
        inputProps={{ placeholder: "First Name" }}
      />
      <Input
        label="Last name"
        name="lastName"
        inputProps={{ placeholder: "First Name" }}
      />
      <Button.Submit disabled={loading} loading={loading}>
        {buttonText}
      </Button.Submit>
    </Form>
  );
};

export default UserForm;
