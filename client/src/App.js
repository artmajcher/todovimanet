import React from "react";
import { ConnectedRouter } from "connected-react-router";
import Providers from "./providers/Providers";
import { history } from "./_helpers/rootReducer";
import Routes from "./layout/Routes/Routes";
import "dayjs/locale/pl";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import dayjs from "dayjs";

function App() {
  dayjs().locale("pl");
  return (
    <Providers>
      <ConnectedRouter history={history}>
        <Routes />
      </ConnectedRouter>
    </Providers>
  );
}

export default App;
