import React from "react";
import { Header } from "semantic-ui-react";
import "./Footer.css";
const Footer = () => {
  return (
    <footer>
      <Header
        inverted
        content=" © 2019 ebiznesliga.pl | All Rights Reserved"
        as="span"
        textAlign="center"
      />
    </footer>
  );
};

export default Footer;
