import React, { useState } from "react";
import { Button, Confirm, Icon } from "semantic-ui-react";

const ConfirmCallback = ({ disabled, buttonText, content, callback }) => {
  const [open, setOpen] = useState(false);

  return (
    <>
      <Button
        disabled={disabled}
        icon
        size="tiny"
        onClick={() => {
          setOpen(true);
        }}
      >
        <Icon name="trash" /> {buttonText}
      </Button>
      <Confirm
        open={open}
        content={content}
        cancelButton="No"
        confirmButton="Yes"
        onCancel={() => {
          setOpen(false);
        }}
        onConfirm={() => {
          setOpen(false);
          callback();
        }}
      />
    </>
  );
};

export default ConfirmCallback;
