import React, { useContext } from "react";
import { NavLink } from "react-router-dom";
import { Menu, Image } from "semantic-ui-react";
import NavContext from "../../contexts/NavContext";
import "./DashboardMenuHeader.css";
const DashboardMenuHeader = () => {
  const context = useContext(NavContext);
  return (
    <nav>
      <Image
        as={NavLink}
        to={context.homeUrl}
        src="/img/placeholder.png"
        title="ebiznesliga"
        centered
      />

      <Menu inverted className="main-menu" vertical>
        <Menu.Item as={NavLink} to={context.homeUrl} exact position="right">
          Users
        </Menu.Item>
        <Menu.Item as={NavLink} to={context.tasksUrl}>
          Tasks
        </Menu.Item>
      </Menu>
    </nav>
  );
};

export default DashboardMenuHeader;
