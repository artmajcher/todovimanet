import React from "react";
import { useDispatch } from "react-redux";
import { Button } from "semantic-ui-react";
import { goBack } from "connected-react-router";

const NavButtons = ({ onReload, active }) => {
  const dispatch = useDispatch();
  return (
    <>
      <Button.Group basic size="small">
        <Button
          icon="chevron circle left"
          disable={active}
          loading={active}
          onClick={() => dispatch(goBack())}
        />
        <Button
          icon="redo"
          disable={active}
          loading={active}
          onClick={() => onReload()}
        />
      </Button.Group>
    </>
  );
};

export default NavButtons;
