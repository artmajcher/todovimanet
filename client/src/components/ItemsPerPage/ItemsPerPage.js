import React, { useState, useEffect } from "react";
import { Button } from "semantic-ui-react";
const ItemsPerPage = ({ itemsChange }) => {
  const [count, setCount] = useState(10);

  const useMountEffect = fun => useEffect(fun, [count]);
  useMountEffect(() => itemsChange(count));

  return (
    <Button.Group size="tiny" floated="right">
      <Button primary={count === 10} onClick={() => setCount(10)}>
        10
      </Button>
      <Button primary={count === 50} onClick={() => setCount(50)}>
        50
      </Button>
      <Button primary={count === 100} onClick={() => setCount(100)}>
        100
      </Button>
    </Button.Group>
  );
};

export default ItemsPerPage;
