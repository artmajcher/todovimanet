import React from "react";

export default React.createContext({
  homeUrl: "",
  usersUrl: "",
  tasksUrl: ""
});
