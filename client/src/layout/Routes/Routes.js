import React, { useContext } from "react";
import { Switch } from "react-router-dom";
import NavContext from "../../contexts/NavContext";
import { AppRoute } from "../../_helpers/customRoutes";
import AdminLayout from "../../layout/AdminLayout/AdminLayout";
import UsersPage from "../../pages/users/UsersPage";
import UserAddPage from "../../pages/users/UserAddPage";
import UserEditPage from "../../pages/users/UserEditPage";
import TaskGroupAddPage from "../../pages/tasks/TaskGroupAddPage";
import TaskGroupEditPage from "../../pages/tasks/TaskGroupEditPage";
import TaskGroupsPage from "../../pages/tasks/TaskGroupsPage";

const Routes = ({ children }) => {
  const context = useContext(NavContext);

  return (
    <Switch>
      <AppRoute
        exact
        path={context.homeUrl}
        component={UsersPage}
        layout={AdminLayout}
      />
      <AppRoute
        exact
        path={context.usersUrl}
        component={UsersPage}
        layout={AdminLayout}
      />
      <AppRoute
        exact
        path={context.usersUrl + "/add"}
        component={UserAddPage}
        layout={AdminLayout}
      />
      <AppRoute
        exact
        path={context.usersUrl + "/:id"}
        component={UserEditPage}
        layout={AdminLayout}
      />
      <AppRoute
        exact
        path={context.tasksUrl}
        component={TaskGroupsPage}
        layout={AdminLayout}
      />
      <AppRoute
        exact
        path={context.tasksUrl + "/add"}
        component={TaskGroupAddPage}
        layout={AdminLayout}
      />
      <AppRoute
        exact
        path={context.tasksUrl + "/:id"}
        component={TaskGroupEditPage}
        layout={AdminLayout}
      />
    </Switch>
  );
};

export default Routes;
