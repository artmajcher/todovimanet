import { HEADER_SETUP } from "./types";

export const setupHeader = () => {
  return dispatch => {
    dispatch(submitRequest());
  };
};

const submitRequest = () => ({
  type: HEADER_SETUP
});
