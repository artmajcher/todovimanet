import { HEADER_SETUP } from "./types";

const initialState = {
  authHeaderSetup: false
};

export default function authHeaderReducer(state = initialState, action) {
  switch (action.type) {
    case HEADER_SETUP:
      return {
        authHeaderSetup: true
      };
    default:
      return state;
  }
}
