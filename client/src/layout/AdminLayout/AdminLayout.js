import React from "react";
import { Segment } from "semantic-ui-react";
import DashboardMenuHeader from "../../components/DashboardMenuHeader/DashboardMenuHeader";
import Errors from "../../pages/errors/Errors";
import "./AdminLayout.css";
const AdminLayout = props => {
  return (
    <React.Fragment>
      <div className="admin-menu">
        <DashboardMenuHeader />
      </div>
      <div className="admin-content">
        <Errors />
        <Segment>{props.children}</Segment>
      </div>
    </React.Fragment>
  );
};

export default AdminLayout;
