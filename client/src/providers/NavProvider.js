import React from "react";
import NavContext from "./../contexts/NavContext";
const NavProvider = props => {
  return (
    <NavContext.Provider
      value={{
        homeUrl: "/",
        usersUrl: "/users",
        tasksUrl: "/tasks"
      }}
    >
      {props.children}
    </NavContext.Provider>
  );
};

export default NavProvider;
