import React from "react";
import { Provider } from "react-redux";
import NavProvider from "./NavProvider";
import store from "../_helpers/store";

const Providers = props => {
  return (
    <Provider store={store}>
      <NavProvider>{props.children}</NavProvider>
    </Provider>
  );
};

export default Providers;
