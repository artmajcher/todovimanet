export const FULL_DATE_FORMAT = "DD.MM.YYYY HH:mm:ss";
export const DATE_WITH_HOUR_FORMAT = "HH:mm DD.MM.YYYY";
export const DATE_FORMAT = "DD.MM.YYYY";
export const HOUR_FORMAT = "HH:mm";
export const FULL_HOUR_FORMAT = "HH:mm:ss";
