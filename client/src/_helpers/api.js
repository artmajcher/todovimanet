export const API_URL = "http://localhost:5000";
// export const AUTH_URL = process.env.REACT_APP_AUTH_URL
//   ? process.env.REACT_APP_AUTH_URL
//   : "window._env_.AUTH_URL";
// export const CLIENT_URL = process.env.REACT_APP_URL
//   ? process.env.REACT_APP_URL
//   : "window._env_.REACT_APP_URL";

export default {
  users: API_URL + "/users",
  tasks: API_URL + "/taskGroups"
};
