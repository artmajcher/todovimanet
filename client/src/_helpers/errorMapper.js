export function mapErrorToList(data) {
  if (typeof data === "object") {
    if (Array.isArray(data)) {
      let values = data.map(e => {
        if (e.description) return e.description;
        else return e;
      });
      return Object.values(values);
    }
    if (data.errors) {
      return Object.values(data.errors);
    }
    return Object.values(data);
  }
  let values = data.map(e => {
    if (e.description) return e.description;
    else return e;
  });
  return Object.values(values);
}
