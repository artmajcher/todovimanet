import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import { rootReducer } from "./rootReducer";
import { history } from "./rootReducer";
import { routerMiddleware } from "connected-react-router";

//const oidcMiddleware = createOidcMiddleware(userManager);
const loggerMiddleware = store => next => action => {
  console.log("Action type:", action.type);
  next(action);
};

const initialState = {};

const createStoreWithMiddleware = compose(
  applyMiddleware(thunk, loggerMiddleware, routerMiddleware(history))
)(createStore);

const store = createStoreWithMiddleware(rootReducer, initialState);

export default store;
