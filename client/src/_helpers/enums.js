export const userTaskStatus = {
  New: 0,
  InProgress: 1,
  Completed: 2
};

export const getUserTaskStatusName = status => {
  switch (status) {
    case userTaskStatus.New:
      return "New";
    case userTaskStatus.InProgress:
      return "In Progress";
    case userTaskStatus.Completed:
      return "Completed";
    default:
      return "";
  }
};

export const getDropDownListItems = (from, to, parser) => {
  const st = [];
  for (let i = from; i <= to; i++) {
    st.push({ value: i, text: parser(i) });
  }
  return st;
};
