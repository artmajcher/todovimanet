import React, { useContext } from "react";
import { Route, Redirect, withRouter, NavLink, Link } from "react-router-dom";
import NavContext from "./../contexts/NavContext";

export const PrivateRoute = ({
  component: Component,
  sidebar,
  submenu,
  layout: Layout,
  ...rest
}) => {
  const navContext = useContext(NavContext);
  return (
    <Route
      {...rest}
      render={props => (
        <Layout sidebar={sidebar} submenu={submenu}>
          {localStorage.getItem("user") || sessionStorage.getItem("user") ? (
            <Component {...props} />
          ) : (
            <Redirect
              to={{
                pathname: navContext.loginUrl,
                state: { from: props.location }
              }}
            />
          )}
        </Layout>
      )}
    />
  );
};
export const RestrictedMembershipRoute = ({
  component: Component,
  sidebar,
  submenu,
  layout: Layout,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => (
      <Layout sidebar={sidebar} submenu={submenu}>
        {localStorage.getItem("user") || sessionStorage.getItem("user") ? (
          <Component {...props} />
        ) : (
          <React.Fragment />
        )}
      </Layout>
    )}
  />
);

export const RestrictedRoute = ({
  component: Component,
  sidebar,
  submenu,
  layout: Layout,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => (
      <Layout sidebar={sidebar} submenu={submenu}>
        {localStorage.getItem("user") || sessionStorage.getItem("user") ? (
          <Component {...props} />
        ) : (
          <React.Fragment />
        )}
      </Layout>
    )}
  />
);
export const AppRoute = ({
  component: Component,
  sidebar,
  submenu,
  header,
  layout: Layout,
  ...rest
}) => (
  <Route
    exact
    {...rest}
    render={props => (
      <Layout>
        <Component {...props} />
      </Layout>
    )}
  />
);

export const DashboardRoute = ({
  component: Component,
  sidebar: Sidebar,
  layout: Layout,
  ...rest
}) => (
  <Route
    {...rest}
    render={props => (
      <Layout sidebar={Sidebar}>
        <Component {...props} />
      </Layout>
    )}
  />
);
export const Nav = props => (
  <NavLink exact {...props} activeClassName="active" />
);

class SafeLink extends React.Component {
  onClick(event) {
    if (this.props.to === this.props.history.location.pathname) {
      event.preventDefault();
    }
    // Ensure that if we passed another onClick method as props, it will be called too
    if (this.props.onClick) {
      this.props.onClick();
    }
  }

  render() {
    const { children, onClick, staticContext, ...other } = this.props;
    return (
      <Link onClick={this.onClick.bind(this)} {...other}>
        {children}
      </Link>
    );
  }
}

export default withRouter(SafeLink);
