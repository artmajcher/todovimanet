export function pushToArray(arr, arr2) {
  for (let i = 0; i < arr2.length; i++) {
    const index = arr.findIndex(e => e.id === arr2[i].id);
    if (index === -1) {
      arr.push(arr2[i]);
    } else {
      arr[index] = arr2[i];
    }
  }
}
export function arrayUnique(array) {
  var a = array.concat();
  for (var i = 0; i < a.length; ++i) {
    for (var j = i + 1; j < a.length; ++j) {
      if (a[i] === a[j]) a.splice(j--, 1);
    }
  }

  return a;
}
