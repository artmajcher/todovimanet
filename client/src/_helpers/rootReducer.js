import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import { createBrowserHistory } from "history";
import usersReducer from "../pages/users/redux/reducers";
import tasksReducer from "../pages/tasks/redux/reducers";
import errorsReducer from "../pages/errors/redux/reducers";

export const history = createBrowserHistory();

export const rootReducer = combineReducers({
  usersReducer,
  tasksReducer,
  errorsReducer,
  router: connectRouter(history)
});
