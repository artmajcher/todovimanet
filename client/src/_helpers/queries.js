export function addOrUpdateById(array, item) {
  let current = array;
  if (!current) current = [];
  if (!item) return current;

  let currentItem = current.find(i => i.id === item.id);
  if (!currentItem) {
    current.push(item);
    return current;
  } else {
    return current.map(m => {
      return m.id === item.id ? item : m;
    });
  }
}
export function updateById(array, item) {
  let current = array;
  if (!current) current = [];
  if (!item) return current;

  let currentItem = current.find(i => i.id === item.id);
  if (currentItem) {
    return current.map(m => {
      return m.id === item.id ? item : m;
    });
  }
  return current;
}
