namespace ToDoExam.Domain.Shared
{
    public abstract class Entity
    {
        public int Id { get; set; }
    }
}