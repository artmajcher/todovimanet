using System;

namespace ToDoExam.Domain.Shared
{
    public abstract class Deletable : Entity
    {
        public DateTime DeletedAt { get; private set; }
        public bool IsDeleted { get; private set; }

        public virtual void SoftDelete()
        {
            DeletedAt = DateTime.UtcNow;
            IsDeleted = true;
        }
    }
}