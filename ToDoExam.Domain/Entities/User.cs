using System;
using ToDoExam.Domain.Shared;

namespace ToDoExam.Domain.Entities
{
    public class User : Entity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        private User() { }

        public User(string firstName, string lastName) : this()
        {
            FirstName = firstName;
            LastName = lastName;
        }

        public void ChangeName(string firstName, string lastName)
        {
            FirstName = firstName;
            LastName = lastName;
        }
    }
}