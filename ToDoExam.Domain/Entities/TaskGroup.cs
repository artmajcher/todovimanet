using System;
using System.Collections.Generic;
using ToDoExam.Domain.Shared;

namespace ToDoExam.Domain.Entities
{
    public class TaskGroup : Deletable
    {
        public string Name { get; private set; }
        public ICollection<UserTask> Tasks { get; private set; }

        private TaskGroup()
        {
            Tasks = new HashSet<UserTask>();
        }
        public TaskGroup(string name) : this()
        {
            Name = name;
        }
        public void AddTask(UserTask task)
        {
            Tasks.Add(task);
        }
        public void ClearTasks()
        {
            Tasks.Clear();
        }

        public void ChangeName(string name)
        {
            Name = name;
        }

        public override void SoftDelete()
        {
            base.SoftDelete();
            foreach (var task in Tasks)
            {
                task.SoftDelete();
            }
        }
    }
}