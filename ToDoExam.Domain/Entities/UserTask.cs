using System;
using ToDoExam.Domain.Shared;

namespace ToDoExam.Domain.Entities
{
    public enum UserTaskStatus
    {
        New,
        InProgress,
        Completed
    }
    public class UserTask : Deletable
    {
        public string Name { get; private set; }
        public DateTime Deadline { get; private set; }
        public UserTaskStatus Status { get; private set; }
        public int? UserId { get; private set; }
        public User User { get; private set; }

        private UserTask()
        {
            //For tests in real logic DateTime should be abstract 
            Deadline = DateTime.Now;
            Status = UserTaskStatus.New;
        }
        public UserTask(string name, DateTime deadline, UserTaskStatus status, int? userId) : this()
        {
            Name = name;
            Deadline = deadline;
            Status = status;
            UserId = userId;
        }
        public void ChangeStatus(UserTaskStatus newStatus)
        {
            Status = newStatus;
        }
    }
}