using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ToDoExam.Domain.Entities;

namespace ToDoExam.Data.Configuration
{
    public class TaskGroupConfiguration : IEntityTypeConfiguration<TaskGroup>
    {
        public void Configure(EntityTypeBuilder<TaskGroup> builder)
        {
            builder.HasQueryFilter(e => !e.IsDeleted);
        }
    }
}