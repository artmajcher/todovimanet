using System.IO;

namespace ToDoExam.Data.Constants
{
    public static class LocalPath
    {
        public static string ResourcesDir = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
        public static string TeamsLogoDir = Path.Combine("resources", "images");
        public static string PlayersLogoDir = Path.Combine("resources", "images");

    }

}