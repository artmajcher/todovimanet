## How to run a project

To run a backend move to `ToDoExam.API` and type
`dotnet run`
Make sure that it runs localy on port 5000.

To run a client move to `client`directory and run:
`npm start`
Make sure that it runs localy on port 3000.

For other ports make changes in files
ToDoExam.API\Program.cs
and
client\src_helpers\api.js
