using System;
using System.ComponentModel.DataAnnotations;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using ToDoExam.Application.Exceptions;
using ValidationException = ToDoExam.Application.Exceptions.ValidationException;

namespace ToDoExam.API.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            var code = HttpStatusCode.InternalServerError;
            switch (context.Exception)
            {
                case ValidationException exp:
                    code = HttpStatusCode.BadRequest;
                    context.Result = new JsonResult(((ValidationException) exp).Failures);
                    break;
                case NotFoundException exp:
                    code = HttpStatusCode.NotFound;
                    break;
            }
            context.HttpContext.Response.ContentType = "application/json";
            context.HttpContext.Response.StatusCode = (int) code;
            context.Result = new JsonResult(new
            {
                context.Exception.Message
                // stackTrace = context.Exception.StackTrace
            });
        }
    }
}