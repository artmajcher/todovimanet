using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ToDoExam.Application.Interfaces;
using ToDoExam.API.Filters;
using ToDoExam.Data;

namespace ToDoExam.API.Configuration
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCustomDbContext(this IServiceCollection services, IConfiguration config)
        {

            services.AddDbContext<ToDoExamDbContext>(opts => opts.UseInMemoryDatabase("testdb"));

            services.AddScoped<IToDoExamDbContext>(provider => provider.GetService<ToDoExamDbContext>());
            return services;
        }

        public static IServiceCollection AddCustomMvc(this IServiceCollection services)
        {
            services.AddMvcCore(o => o.Filters.Add(typeof(CustomExceptionFilterAttribute)))

                .AddJsonFormatters()
                .SetCompatibilityVersion(CompatibilityVersion.Version_2_2).AddJsonOptions(options =>
                {
                    options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                }).AddFluentValidation(fv => fv.RegisterValidatorsFromAssemblyContaining<IToDoExamDbContext>());

            return services;
        }
    }
}