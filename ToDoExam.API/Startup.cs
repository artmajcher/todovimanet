﻿using System.Reflection;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using ToDoExam.Application.Infrastructure;
using ToDoExam.Application.Interfaces;
using ToDoExam.API.Configuration;

namespace ToDoExam.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(new Assembly[] { typeof(AutoMapperProfile).GetTypeInfo().Assembly });
            services.AddMediatR(typeof(IToDoExamDbContext).GetTypeInfo().Assembly);
            services.AddCustomDbContext(Configuration);
            services.AddCustomMvc();
            services.AddSignalR().AddJsonProtocol(options =>
            {
                options.PayloadSerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
            });

            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestValidationBehavior<,>));
            services.AddCors();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseForwardedHeaders();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseCors(builder =>
                    builder.WithOrigins("http://localhost:3000").AllowAnyHeader().AllowAnyMethod());
            }
            else
            {
                app.UseHsts();
            }
            app.UseStaticFiles();

            app.UseMvc();
        }
    }
}