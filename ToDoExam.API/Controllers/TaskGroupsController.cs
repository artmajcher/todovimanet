using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ToDoExam.Application.TaskGroups.Commands.AddTaskGroup;
using ToDoExam.Application.TaskGroups.Commands.DeleteTaskGroup;
using ToDoExam.Application.TaskGroups.Commands.EditTaskGroup;
using ToDoExam.Application.TaskGroups.Queries.GetAllTaskGroups;
using ToDoExam.Application.TaskGroups.Queries.GetTaskGroup;

namespace ToDoExam.API.Controllers
{
    public class TaskGroupsController : BaseController
    {

        [HttpGet]
        public async Task<ActionResult<GetAllTaskGroupsQuery>> Get()
        {
            return Ok(await Mediator.Send(new GetAllTaskGroupsQuery()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<string>> Get(int id)
        {
            return Ok(await Mediator.Send(new GetTaskGroupQuery { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddTaskGroupCommand command)
        {
            return Ok(await Mediator.Send(command));
        }

        [HttpPut]
        public async Task<IActionResult> Edit(EditTaskGroupCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Remove(int id)
        {
            await Mediator.Send(new DeleteTaskGroupCommand { Id = id });

            return NoContent();
        }

    }
}