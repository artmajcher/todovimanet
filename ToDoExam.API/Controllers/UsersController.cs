using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ToDoExam.Application.Users.Commands.AddUser;
using ToDoExam.Application.Users.Commands.DeleteUser;
using ToDoExam.Application.Users.Commands.EditUser;
using ToDoExam.Application.Users.Queries.GetAllUsers;
using ToDoExam.Application.Users.Queries.GetUser;

namespace ToDoExam.API.Controllers
{
    public class UsersController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult<GetAllUsersQuery>> Get()
        {
            return Ok(await Mediator.Send(new GetAllUsersQuery()));
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<GetUserQuery>> Get(int id)
        {
            return Ok(await Mediator.Send(new GetUserQuery { Id = id }));
        }

        [HttpPost]
        public async Task<IActionResult> Add(AddUserCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpPut]
        public async Task<IActionResult> Edit(EditUserCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> Remove(int id)
        {
            await Mediator.Send(new DeleteUserCommand { Id = id });

            return NoContent();
        }
    }
}