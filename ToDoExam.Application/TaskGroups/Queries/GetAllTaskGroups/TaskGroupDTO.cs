using AutoMapper;
using ToDoExam.Application.Interfaces;
using ToDoExam.Domain.Entities;

namespace ToDoExam.Application.TaskGroups.Queries.GetAllTaskGroups
{
    public class TaskGroupDTO : IHaveCustomMapping
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int TasksCount { get; set; }
        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<TaskGroup, TaskGroupDTO>()
                .ForMember(vm => vm.TasksCount, opt => opt.MapFrom(u => u.Tasks.Count));

        }
    }
}