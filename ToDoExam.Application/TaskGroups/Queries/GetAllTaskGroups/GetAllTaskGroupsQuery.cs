using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ToDoExam.Application.Interfaces;
using ToDoExam.Domain.Entities;

namespace ToDoExam.Application.TaskGroups.Queries.GetAllTaskGroups
{
    public class GetAllTaskGroupsQuery : IRequest<TaskGroupsListVM>
    {
        public class Handler : IRequestHandler<GetAllTaskGroupsQuery, TaskGroupsListVM>
        {
            protected readonly IToDoExamDbContext _context;
            protected readonly IMapper _mapper;
            public Handler(IToDoExamDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<TaskGroupsListVM> Handle(GetAllTaskGroupsQuery request, CancellationToken cancellationToken)
            {
                return new TaskGroupsListVM
                {
                    TaskGroups = await _context.TaskGroups.ProjectTo<TaskGroupDTO>(_mapper.ConfigurationProvider).ToListAsync(cancellationToken)
                };
            }
        }
    }
}