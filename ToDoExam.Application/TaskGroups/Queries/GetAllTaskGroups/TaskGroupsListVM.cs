using System.Collections.Generic;

namespace ToDoExam.Application.TaskGroups.Queries.GetAllTaskGroups
{
    public class TaskGroupsListVM
    {
        public IList<TaskGroupDTO> TaskGroups { get; set; }
    }
}