using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ToDoExam.Application.Exceptions;
using ToDoExam.Application.Interfaces;
using ToDoExam.Domain.Entities;

namespace ToDoExam.Application.TaskGroups.Queries.GetTaskGroup
{
    public class GetTaskGroupQuery : IRequest<TaskGroupVM>
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<GetTaskGroupQuery, TaskGroupVM>
        {
            protected readonly IToDoExamDbContext _context;
            protected readonly IMapper _mapper;
            public Handler(IToDoExamDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<TaskGroupVM> Handle(GetTaskGroupQuery request, CancellationToken cancellationToken)
            {
                var user = await _context.TaskGroups.ProjectTo<TaskGroupVM>(_mapper.ConfigurationProvider).SingleOrDefaultAsync(p => p.Id == request.Id) ??
                    throw new NotFoundException(nameof(User), request.Id);

                return user;
            }
        }
    }
}