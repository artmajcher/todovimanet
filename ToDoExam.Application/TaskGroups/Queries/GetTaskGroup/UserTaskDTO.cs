using System;
using AutoMapper;
using ToDoExam.Application.Interfaces;
using ToDoExam.Domain.Entities;

namespace ToDoExam.Application.TaskGroups.Queries.GetTaskGroup
{
    public class UserTaskDTO : IHaveCustomMapping
    {
        public string Name { get; set; }
        public DateTime Deadline { get; set; }
        public UserTaskStatus Status { get; set; }
        public string User { get; set; }
        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<UserTask, UserTaskDTO>()
                .ForMember(vm => vm.User, opt => opt.MapFrom(u => $"{u.User.FirstName} {u.User.LastName}"));
        }
    }
}