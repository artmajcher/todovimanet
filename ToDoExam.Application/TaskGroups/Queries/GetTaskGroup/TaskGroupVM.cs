using System.Collections.Generic;
using AutoMapper;
using ToDoExam.Application.Interfaces;
using ToDoExam.Domain.Entities;

namespace ToDoExam.Application.TaskGroups.Queries.GetTaskGroup
{
    public class TaskGroupVM : IHaveCustomMapping
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IList<UserTaskDTO> Tasks { get; set; }
        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<TaskGroup, TaskGroupVM>();
        }
    }
}