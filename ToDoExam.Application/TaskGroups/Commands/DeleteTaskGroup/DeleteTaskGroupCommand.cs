using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ToDoExam.Application.Interfaces;
using ToDoExam.Domain.Entities;

namespace ToDoExam.Application.TaskGroups.Commands.DeleteTaskGroup
{
    public class DeleteTaskGroupCommand : IRequest
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<DeleteTaskGroupCommand, Unit>
        {
            protected readonly IToDoExamDbContext _context;
            public Handler(IToDoExamDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteTaskGroupCommand request, CancellationToken cancellationToken)
            {
                var taskGroup = await _context.TaskGroups
                    .Include(t => t.Tasks)
                    .FirstOrDefaultAsync(t => t.Id == request.Id);

                taskGroup.SoftDelete();

                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}