using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ToDoExam.Application.Interfaces;
using ToDoExam.Domain.Entities;

namespace ToDoExam.Application.TaskGroups.Commands.AddTaskGroup
{
    public class AddTaskGroupCommand : IRequest
    {
        public string Name { get; set; }
        public List<TaskDto> Tasks { get; set; }

        public class Handler : IRequestHandler<AddTaskGroupCommand, Unit>
        {
            protected readonly IToDoExamDbContext _context;
            public Handler(IToDoExamDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(AddTaskGroupCommand request, CancellationToken cancellationToken)
            {
                var taskgroup = new TaskGroup(request.Name);

                foreach (var u in request.Tasks)
                {
                    taskgroup.AddTask(new UserTask(u.Name, u.Deadline, u.Status, u.UserId));
                }

                _context.TaskGroups.Add(taskgroup);

                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}