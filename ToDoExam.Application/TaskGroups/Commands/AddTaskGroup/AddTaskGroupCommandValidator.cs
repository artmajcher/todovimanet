using FluentValidation;

namespace ToDoExam.Application.TaskGroups.Commands.AddTaskGroup
{
    public class AddTaskGroupCommandValidator : AbstractValidator<AddTaskGroupCommand>
    {
        public AddTaskGroupCommandValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleForEach(x => x.Tasks).SetValidator(new TaskDtoValidator());

        }
    }
    public class TaskDtoValidator : AbstractValidator<TaskDto>
    {
        public TaskDtoValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.Deadline).NotEmpty();
            RuleFor(x => x.Status).NotEmpty();
        }
    }
}