using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ToDoExam.Application.Interfaces;
using ToDoExam.Domain.Entities;

namespace ToDoExam.Application.TaskGroups.Commands.EditTaskGroup
{
    public class EditTaskGroupCommand : IRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<TaskDto> Tasks { get; set; }

        public class Handler : IRequestHandler<EditTaskGroupCommand, Unit>
        {
            protected readonly IToDoExamDbContext _context;
            protected readonly IMapper _mapper;
            public Handler(IToDoExamDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<Unit> Handle(EditTaskGroupCommand request, CancellationToken cancellationToken)
            {
                var taskGroup = await _context.TaskGroups
                    .Include(t => t.Tasks)
                    .ThenInclude(t => t.User)
                    .FirstOrDefaultAsync(t => t.Id == request.Id);
                //var user = await _context.Users.FindAsync(request.UserId);
                taskGroup.ChangeName(request.Name);
                taskGroup.ClearTasks();
                foreach (var u in request.Tasks)
                {
                    taskGroup.AddTask(new UserTask(u.Name, u.Deadline, u.Status, u.UserId));
                }

                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}