using System;
using ToDoExam.Domain.Entities;

namespace ToDoExam.Application.TaskGroups.Commands.EditTaskGroup
{
    public class TaskDto
    {
        public string Name { get; set; }
        public DateTime Deadline { get; set; }
        public UserTaskStatus Status { get; set; }
        public int? UserId { get; set; }
    }
}