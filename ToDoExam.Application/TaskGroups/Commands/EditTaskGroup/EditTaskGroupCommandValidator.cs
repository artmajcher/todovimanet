using FluentValidation;

namespace ToDoExam.Application.TaskGroups.Commands.EditTaskGroup
{
    public class EditTaskGroupCommandValidator : AbstractValidator<EditTaskGroupCommand>
    {
        public EditTaskGroupCommandValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleForEach(x => x.Tasks).SetValidator(new TaskDtoValidator());

        }
    }
    public class TaskDtoValidator : AbstractValidator<TaskDto>
    {
        public TaskDtoValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.Deadline).NotEmpty();
            RuleFor(x => x.Status).NotEmpty();
        }
    }
}