using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ToDoExam.Domain.Entities;

namespace ToDoExam.Application.Interfaces
{
    public interface IToDoExamDbContext
    {
        DbSet<User> Users { get; set; }
        DbSet<TaskGroup> TaskGroups { get; set; }

        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}