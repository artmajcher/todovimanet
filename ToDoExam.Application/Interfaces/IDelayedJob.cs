using System;

namespace ToDoExam.Application.Interfaces
{
    public interface IDelayedJob
    {
        void MatchUpdate(TimeSpan date, int matchId);
    }
}