using Microsoft.EntityFrameworkCore;

namespace ToDoExam.Application.Interfaces
{
    public interface IMessageDispatcher
    {
        void SendMessage(string type, string message);
    }
}