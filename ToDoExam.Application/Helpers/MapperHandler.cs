using AutoMapper;
using MediatR;
using ToDoExam.Application.Interfaces;

namespace ToDoExam.Application.Helpers
{
    public abstract class MapperHandler
    {
        protected readonly IToDoExamDbContext Context;
        protected readonly IMapper Mapper;

        public MapperHandler(IToDoExamDbContext context, IMapper mapper)
        {
            this.Context = context;
            this.Mapper = mapper;
        }
    }
}