using X.PagedList;

namespace ToDoExam.Application.Helpers
{
    public abstract class PaginationResult
    {
        public int TotalItemCount { get; private set; }
        public int PageNumber { get; private set; }
        public int PageSize { get; private set; }
        public bool HasPreviousPage { get; private set; }
        public bool HasNextPage { get; private set; }
        public bool IsFirstPage { get; private set; }
        public bool IsLastPage { get; private set; }

        public void SetPaginationResult(IPagedList list)
        {
            TotalItemCount = list.TotalItemCount;
            PageNumber = list.PageNumber;
            PageSize = list.PageSize;
            HasPreviousPage = list.HasPreviousPage;
            HasNextPage = list.HasNextPage;
            IsFirstPage = list.IsFirstPage;
            IsLastPage = list.IsLastPage;
        }
    }
}