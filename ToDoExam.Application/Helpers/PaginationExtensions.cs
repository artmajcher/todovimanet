using System.Linq;
using System.Threading.Tasks;
using X.PagedList;

namespace ToDoExam.Application.Helpers
{
    public static class PaginationExtensions
    {
        public static async Task<IPagedList<T>> ToPagedListAsync<T>(this IQueryable<T> superset, PaginationQuery request)
        {
            return await superset.ToPagedListAsync(request.PageNumber, request.PageSize);
        }
    }
}