namespace ToDoExam.Application.Helpers
{
    public abstract class PaginationQuery
    {
        public int PageNumber { get; set; } = 1;
        public int PageSize { get; set; } = 10;
        public string OrderBy { get; set; } = "id";

    }
}