using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ToDoExam.Application.Interfaces;

namespace ToDoExam.Application.Users.Queries.GetAllUsers
{
    public class GetAllUsersQuery : IRequest<UsersListVM>
    {
        public class Handler : IRequestHandler<GetAllUsersQuery, UsersListVM>
        {
            protected readonly IToDoExamDbContext _context;
            protected readonly IMapper _mapper;
            public Handler(IToDoExamDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<UsersListVM> Handle(GetAllUsersQuery request, CancellationToken cancellationToken)
            {
                return new UsersListVM
                {
                    Users = await _context.Users.ProjectTo<UserDTO>(_mapper.ConfigurationProvider).ToListAsync(cancellationToken)
                };
            }
        }
    }
}