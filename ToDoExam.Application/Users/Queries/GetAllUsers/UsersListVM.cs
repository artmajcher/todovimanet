using System.Collections.Generic;

namespace ToDoExam.Application.Users.Queries.GetAllUsers
{
    public class UsersListVM
    {
        public IList<UserDTO> Users { get; set; }
    }
}