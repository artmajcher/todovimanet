using AutoMapper;
using ToDoExam.Application.Interfaces;
using ToDoExam.Domain.Entities;

namespace ToDoExam.Application.Users.Queries.GetUser
{
    public class UserVM : IHaveCustomMapping
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public void CreateMappings(Profile configuration)
        {
            configuration.CreateMap<User, UserVM>();
        }
    }
}