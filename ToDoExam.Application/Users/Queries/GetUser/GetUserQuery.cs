using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ToDoExam.Application.Exceptions;
using ToDoExam.Application.Interfaces;
using ToDoExam.Domain.Entities;

namespace ToDoExam.Application.Users.Queries.GetUser
{
    public class GetUserQuery : IRequest<UserVM>
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<GetUserQuery, UserVM>
        {
            protected readonly IToDoExamDbContext _context;
            protected readonly IMapper _mapper;
            public Handler(IToDoExamDbContext context, IMapper mapper)
            {
                _context = context;
                _mapper = mapper;
            }

            public async Task<UserVM> Handle(GetUserQuery request, CancellationToken cancellationToken)
            {
                var user = await _context.Users.ProjectTo<UserVM>(_mapper.ConfigurationProvider).SingleOrDefaultAsync(p => p.Id == request.Id) ??
                    throw new NotFoundException(nameof(User), request.Id);

                return user;
            }
        }
    }
}