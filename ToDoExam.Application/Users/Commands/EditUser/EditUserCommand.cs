using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ToDoExam.Application.Exceptions;
using ToDoExam.Application.Interfaces;
using ToDoExam.Domain.Entities;

namespace ToDoExam.Application.Users.Commands.EditUser
{
    public class EditUserCommand : IRequest
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public class Handler : IRequestHandler<EditUserCommand, Unit>
        {
            protected readonly IToDoExamDbContext _context;
            public Handler(IToDoExamDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(EditUserCommand request, CancellationToken cancellationToken)
            {
                var user = _context.Users.Find(request.Id) ??
                    throw new NotFoundException(nameof(User), request.Id);

                user.ChangeName(request.FirstName, request.LastName);

                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}