using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ToDoExam.Application.Interfaces;
using ToDoExam.Domain.Entities;

namespace ToDoExam.Application.Users.Commands.AddUser
{
    public class AddUserCommand : IRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public class Handler : IRequestHandler<AddUserCommand, Unit>
        {
            protected readonly IToDoExamDbContext _context;
            public Handler(IToDoExamDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(AddUserCommand request, CancellationToken cancellationToken)
            {
                _context.Users.Add(new User(request.FirstName, request.LastName));

                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}