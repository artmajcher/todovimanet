using System.Threading;
using System.Threading.Tasks;
using MediatR;
using ToDoExam.Application.Exceptions;
using ToDoExam.Application.Interfaces;
using ToDoExam.Domain.Entities;

namespace ToDoExam.Application.Users.Commands.DeleteUser
{
    public class DeleteUserCommand : IRequest
    {
        public int Id { get; set; }

        public class Handler : IRequestHandler<DeleteUserCommand, Unit>
        {
            protected readonly IToDoExamDbContext _context;
            public Handler(IToDoExamDbContext context)
            {
                _context = context;
            }

            public async Task<Unit> Handle(DeleteUserCommand request, CancellationToken cancellationToken)
            {
                var user = _context.Users.Find(request.Id) ??
                    throw new NotFoundException(nameof(User), request.Id);

                _context.Users.Remove(user);

                await _context.SaveChangesAsync(cancellationToken);
                return Unit.Value;
            }
        }
    }
}